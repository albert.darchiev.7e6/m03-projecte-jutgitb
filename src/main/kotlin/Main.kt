import java.util.*

//Albert Darchiev
//M03 UF4 Projecte troncal v1

val scanner = Scanner(System.`in`)
class Problema(val enunciat: String, val jocProvesPub: JocProves, val jocProvesPriv: JocProves, var resolt: Boolean, var intents: MutableList<String>)
class JocProves(val entrada: String, val sortida: String)

fun main() {
    var problemes = listOf(Problema("Pasa de segons a hores les seguents seqüències de números i suma les hores entre si...", JocProves("405313200 | 79300800 | 3963600", "135716"), JocProves("444441600 | 524044800 | 13312800","272722"), false, mutableListOf()),
        Problema("Conta les letres que estan en majúscula de la seqüència de caràcters..." ,JocProves("DEmá és DiMEcRes", "6"), JocProves("En eL MoDul dE ProGRAmaCió aPrENeM a PROgRaMAR kOTliN AMb JoRDi","29"), false, mutableListOf()),
        Problema("Inverteix la sequéncia de caràcters...", JocProves(".titrevni atse on txet tseuqA", "Aquest text no esta invertit."), JocProves(".scial i scitsàiselce snasetroc snugla bma tnasrevnoc avatse acranom le no laier ólas led adartne’l a àtneserp se ,atiuc-erroc a ,euq seidràug ed àtipac le en-ramrofni rep lograc ed alacse’l àllavad atiaug nU", "Un guaita davallà l’escala de cargol per informar-ne el capità de guàrdies que, a corre-cuita, es presentà a l’entrada del saló reial on el monarca estava conversant amb alguns cortesans eclesiàstics i laics."),false, mutableListOf(),),
        Problema("Conta cuantes paraules hi han a la cadena de caràcters...", JocProves("Avui es dimecres", "3"), JocProves("Kotlin és un llenguatge de programació de propòsit general, multiplataforma i multiparadigma. La seva característica més destacada és comptar amb inferència de tipus. Està dissenyat per poder interoperar completament amb Java tant amb la biblioteca de classes de Java com amb la màquina virtual de Java (JVM). Gràcies a la inferència de tipus, la seva sintaxi es més concisa que la de Java.", "63"),false, mutableListOf()),
        Problema("Indica cuantes vegades apareix \"123\" en la sequéncia de caràcters...", JocProves("221233123", "2"), JocProves("12331212123323212312332212311233212123123123322122312323222331232121231232321233213112331221123123123332112332321233232123","21"),false, mutableListOf())
    )

    problemes.forEachIndexed { index, problema ->
        println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv PROBLEMA ${index+1} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n")
        println(problema.enunciat)
        println("ENTRADA: "+problema.jocProvesPub.entrada)
        println("SORTIDA: "+problema.jocProvesPub.sortida)
        println("\nVols resoldre el problema?  SI | NO ")
        if (scanner.next().uppercase() == "SI") solveProblema(problema)
    }
}

fun solveProblema(problema: Problema){
    println("\nENTRADA: "+problema.jocProvesPriv.entrada)
    println("SORTIDA: ")
    val resposta = scanner.next()
    if (resposta == problema.jocProvesPriv.sortida) {
        problema.resolt = true
        println("CORRECTE!")
    }
    else {
        problema.intents.add(resposta)
        (println("INCORRECTE. Vols tornar a intentar-ho?  SI | NO"))
        if (scanner.next().uppercase() == "SI") solveProblema(problema)
    }
}
